Precondiciones:
Tener inslatado python3 y una distribucion de linux

Ejecución:
Ejectuar los siguiente comando dentro de la carpeta mytodolist,
donde se encuentra el archivo manage.py
python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py loaddata auth.json
python3 manage.py loaddata workers.json
python3 manage.py loaddata todolist.json
python3 manage.py runserver

App:
Ingresar por un navegador a localhost:8000



