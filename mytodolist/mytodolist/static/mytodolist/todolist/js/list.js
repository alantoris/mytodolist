$(document).ready(function(e) {

	loadTasks();

    var date = new Date();
    var today = new Date(date.getFullYear(), date.getMonth(), date.getDate(),0,0,0);

    $('#due_date').datepicker({
        useCurrent: false,
        sideBySide:true,
        format: 'dd/mm/yyyy',
        autoclose: true,
    });

    $('#btnCreateTask').click(function() {
    	createEditTask();
	});

	$('#btnDeleteTask').click(function() {
		deleteTask();
	});

	$('#btn-create').click(function() {
		$('#id-task').val('-1');
	});
});

function loadTasks(){
	$.ajax({
        url: url_ws_load_tasks,
        type: 'get',
        dataType: "json",
        success: function(data, status, jqXHR) {
        	$('#tasks-cointainer').html("");
        	$.each(data.data, function( index, value ) {
        		row = '<div class="row"><div class="col-md-12"><input type="checkbox" ';
        		if (value['done']){
        			row += 'checked ';
        		}
        		row += ' disabled> <label class="label-mytodo">';
        		row += value['title'];
        		row += '</label><i class="fa fa-trash btnDeleteTask" aria-hidden="true" ';
        		row += ' id-task="' + value['id'] + '" ';
        		row += ' data-toggle="modal" data-target="#delete-Modal" '
        		row += '></i><i class="fa fa-pencil btnEditTask" aria-hidden="true" ';
        		row += ' description="' + value['description'] + '"';
        		row += ' title="' + value['title'] + '"';
        		row += ' due_date="' + value['due_date'] + '"';
        		row += ' done="' + value['done'] + '"';
        		row += ' id-task="' + value['id'] + '"';
        		row += ' data-toggle="modal" data-target="#create-edit-Modal"';
        		row += '></i></div></div>';
				$('#tasks-cointainer').append(row);
			});

			$('.btnEditTask').click(function() {
				$('#task-label').val($(this).attr('title'));
				$('#due_date').val($(this).attr('due_date'));
				$('#description').val($(this).attr('description'));
				$('#done').attr('checked', $(this).attr('done'));
				$('#id-task').val($(this).attr('id-task'));
			});

			$('.btnDeleteTask').click(function() {
				$('#id-delete-task').val($(this).attr('id-task'));
			});
        },
        error: function(msg, status, jqXHR) {
            alertify.error("Internal error...");
        },
    });
}

function createEditTask(){
	if (($('#task-label').val() != "") & ($('#due_date').val() != "") &($('#description').val() != "")){
		$('#task-label').css('border-color','#ccc');
		$('#due_date').css('border-color','#ccc');
		$('#description').css('border-color','#ccc');
	    $.ajax({
	        url: url_ws_create_edit_task,
	        type: 'post',
	        data: {
	            'title': $('#task-label').val(),
	            'due_date': $('#due_date').val(),
	            'description': $('#description').val(),
	            'done': $('#done').is(":checked"),
	            'id': $('#id-task').val(),
	            'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
	        },
	        dataType: "json",
	        success: function(data, status, jqXHR) {
	        	loadTasks();
	        	$('#create-edit-Modal').modal('toggle');
	        	if ($('#id-task').val() == "-1"){
	        		alertify.success("The task has been created.");
	        	}
	        	else{
	        		alertify.success("The task has been edited.");
	        	}
	        	
	        },
	        error: function(msg, status, jqXHR) {
	            alertify.error("Internal error...");
	        },
	    });
	}
	else{
		if ($('#task-label').val() == ""){
			$('#task-label').css('border-color','red');
		}
		else{
			$('#task-label').css('border-color','#ccc');
		}
		if ($('#due_date').val() == ""){
			$('#due_date').css('border-color','red');
		}
		else{
			$('#due_date').css('border-color','#ccc');
		}
		if ($('#description').val() == ""){
			$('#description').css('border-color','red');
		}
		else{
			$('#description').css('border-color','#ccc');
		}
		alertify.error("All the field are required.");
	}
}

function deleteTask(){
	if ($('#id-delete-task').val() != ""){
	    $.ajax({
	        url: url_ws_delete_task,
	        type: 'post',
	        data: {
	            'id': $('#id-delete-task').val(),
	            'csrfmiddlewaretoken': $("input[name='csrfmiddlewaretoken']").val()
	        },
	        dataType: "json",
	        success: function(data, status, jqXHR) {
	        	loadTasks();
	        	$('#delete-Modal').modal('toggle');
	        	alertify.success("The task has been deleted.");
	        },
	        error: function(msg, status, jqXHR) {
	            alertify.error("Internal error...");
	        },
	    });
	}
}
