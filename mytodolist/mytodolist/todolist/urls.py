from django.conf.urls import url

from .views import List
from .views import CreateTask
from .views import LoadTasks
from .views import DeleteTask

urlpatterns = [

    url(
        r'^$',
        List,
        name='list'),

    url(
    	r'^create_task/',
    	CreateTask,
    	name='ws_create_edit_task'),

    url(
    	r'^load_tasks/',
    	LoadTasks,
    	name='ws_load_tasks'),

    url(
    	r'^delete_tasks/',
    	DeleteTask,
    	name='ws_delete_task'),

]
