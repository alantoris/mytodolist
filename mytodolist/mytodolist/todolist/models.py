from django.db import models
from workers.models import Worker

# Create your models here.
class ToDoList(models.Model):
    worker = models.ForeignKey(Worker)
    creation_date = models.DateTimeField()
    modification_date = models.DateTimeField(blank=True, null=True)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def as_json(self):
        return {'First name': self.worker.firstname, 'Last name': self.worker.lastname,
                'Id List': self.id}

    def __str__(self):
        return str(self.id) + " " + self.worker.firstname + " " + self.worker.lastname


# Create your models here.
class Task(models.Model):
    title = models.CharField(max_length=55)
    description = models.CharField(max_length=255)
    due_date = models.DateField()
    done = models.BooleanField()
    todolist = models.ForeignKey(ToDoList)
    creation_date = models.DateTimeField()
    modification_date = models.DateTimeField(blank=True, null=True)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def as_json(self):
        return {
            'id': self.id, 
            'title': self.title,
            'description': self.description,
            'due_date': self.due_date.strftime("%d/%m/%Y"),
            'done': self.done
        }

    def __str__(self):
        return str(self.id) + self.todolist.worker.firstname + " " + self.todolist.worker.lastname