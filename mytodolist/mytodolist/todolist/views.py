from django.shortcuts import render
from .models import ToDoList
from .models import Task
from workers.models import Worker
import json
from django.http import HttpResponse
from datetime import datetime
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='login')
def List(request):
    tempalte_name = 'list.html'
    ctx = {}
    worker_logged = Worker.objects.get(user=request.user)
    todolist = ToDoList.objects.get(
        worker=worker_logged)
    ctx['task_list'] = Task.objects.filter(todolist=todolist)
    return render(request,tempalte_name,ctx)


@login_required(login_url='login')
def CreateTask(request):
    response = {}

    if request.is_ajax():
        worker_logged = Worker.objects.get(user=request.user)
        todolist = ToDoList.objects.get(
            worker=worker_logged)
        if request.POST.get('done') == 'true':
            done = True
        else:
            done = False

        if request.POST.get('id') == "-1" :
            print('creando')
            task = Task.objects.create(
                title=request.POST.get('title'),
                description=request.POST.get('description'),
                due_date=datetime.strptime(request.POST.get('due_date'), '%d/%m/%Y'),
                done=done,
                todolist=todolist,
                creation_date=datetime.now()
            )
        else:
            print('editando')
            task = Task.objects.get(id=int(request.POST.get('id')))
            task.title = request.POST.get('title')
            task.description = request.POST.get('description')
            task.due_date = datetime.strptime(request.POST.get('due_date'), '%d/%m/%Y')
            task.done = done
            task.modification_date = datetime.now()
            task.save()

    response = json.dumps(response, indent=4, separators=(',', ': '))
    return HttpResponse(response, content_type="application/json")


@login_required(login_url='login')
def LoadTasks(request):
    response = {}
    response["data"] = []

    if request.is_ajax():
        worker_logged = Worker.objects.get(user=request.user)
        todolist = ToDoList.objects.get(
            worker=worker_logged)
        tasks = Task.objects.filter(todolist=todolist)

        for task in tasks:
            response["data"].append(task.as_json())

    response = json.dumps(response, indent=4, separators=(',', ': '))
    return HttpResponse(response, content_type="application/json")


@login_required(login_url='login')
def DeleteTask(request):
    response = {}

    if request.is_ajax():
        id_delete = request.POST.get('id')
        task_delete = Task.objects.get(id=id_delete)
        task_delete.delete()
        
    response = json.dumps(response, indent=4, separators=(',', ': '))
    return HttpResponse(response, content_type="application/json")