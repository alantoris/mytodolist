from django.contrib.auth.models import User


class LoginForm():
    def __init__(self, post):
        self.username = post.get('username', None)
        self.password = post.get('password', None)
        self.error = ''
        self.model = User

    def is_valid(self):
        if self.username and self.password:
            return True
        else:
            self.error = "Todos los campos son obligatorios"
            return False