from django.shortcuts import redirect
from django.shortcuts import render
from .forms import LoginForm
from django.views.generic import View
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import logout

# Create your views here.
class Login(View):
    form_class = LoginForm
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        ctx = {}
        return render(request, self.template_name, ctx)

    def post(self, request, *args, **kwargs):
        ctx = {}
        form = self.form_class(request.POST)
        if form.is_valid():
            user = authenticate(username=form.username, password=form.password)
            if user is not None:
                login(request, user)
                return redirect('list')
            else:
                form.error = "Las credenciales no coinciden"
        ctx['form'] = form
        return render(request, self.template_name, ctx)

def Logout(request):
    logout(request)
    return redirect("login")