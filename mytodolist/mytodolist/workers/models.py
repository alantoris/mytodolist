from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Worker(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    firstname = models.CharField(max_length=45)
    lastname = models.CharField(max_length=45)
    creation_date = models.DateTimeField()
    modification_date = models.DateTimeField(blank=True, null=True)
    deleted_date = models.DateTimeField(blank=True, null=True)

    def as_json(self):
        return {'First name': self.firstname, 'Last name': self.lastname}

    def __str__(self):
        return self.firstname + " " + self.lastname